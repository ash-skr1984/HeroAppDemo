//
//  main.m
//  HeroAppDemo
//
//  Created by Ashish Mathur on 27/11/17.
//  Copyright © 2017 Ashish Mathur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
